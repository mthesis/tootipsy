import numpy as np
import json

f=np.load("data_2.npz",allow_pickle=True)

i=f["i"]
p=f["p"]
m=f["m"]


x,a=[],[]


alli=[]
for ii in i:
  for iii in ii:
    alli.append(iii)



alli=set(alli)

c=len(alli)

alli={ac:i for ac,i in zip(alli,range(1,c+1))}
with open("map.json","w") as f:
  f.write(json.dumps(alli))




for idd,(ii,pp,mm) in enumerate(zip(i,p,m)):
  
  obj=[]

  for iii,mmm in zip(ii,mm):
    ac=np.zeros(c)
    ac[alli[iii]-1]=1.0
    obj.append(ac)
  # for i in range(len(pp)):
    # obj.append(np.zeros(c))
  
  x.append(obj)
  
  continue
  
  lob=len(obj)
  lii=len(ii)
  
  A=np.zeros((lob,lob))
  
  for ip,ppp in enumerate(pp):
    for sp in ppp:
      i1=ip+lii
      i2=sp
      A[i1,i2]=1.0
      A[i2,i1]=1.0
  
  for ic in range(lii,lob-1):
    A[ic,ic+1]=1.0
    A[ic+1,ic]=1.0

  a.append(A)

  print(f"did {idd}")




np.savez_compressed("data_3",x=x,a=a)


