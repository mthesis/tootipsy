import numpy as np
import matplotlib.pyplot as plt

from os import listdir

from draw import *

import sys
id=-1
if len(sys.argv)>1:id=int(sys.argv[-1])

import json
with open("../map.json","r") as f:
  mp=json.loads(f.read())

amp={mp[key]:key for key in mp.keys()}




fns=np.sort(["images/"+q for q in listdir("images/")])


print(fns)


fn=fns[id]

print("loading",fn)


f=np.load(fn)

q=f["q"]

dx,dy=5,5
fix,axs=plt.subplots(dx,dy)

plt.axis("off")

i=0
for ix in range(dx):
  for iy in range(dy):



    draw(q[i],ax=axs[ix,iy])

    #axs[ix,iy].axis("off")

    i+=1
plt.show()


