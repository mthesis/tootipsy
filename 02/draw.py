import numpy as np
import matplotlib.pyplot as plt 

def draw(d,ax=None):
  if ax is None:ax=plt

  phi=np.arange(0,2,2/9)*np.pi
  x,y=np.sin(phi),np.cos(phi)

  ld=int(d.shape[0])

  A=d[:,:ld]

  #print(A.shape,phi.shape,x.shape,y.shape) 
  
  for i in range(ld):
    for j in range(ld):
      if np.abs(A[i,j])<0.3:continue    
      ax.plot([x[i],x[j]],[y[i],y[j]],color="black",alpha=0.1) 

 
  #for i in range(8):
  #  i2=np.argmin(dt[i])
  #  dt[i2,i]=1000
  #  ax.plot([x[i],x[i2]],[y[i],y[i2]],color="black",alpha=0.1)
  
  
  ax.plot(x,y,"o",color="black")
  

if __name__=="__main__":
  f=np.load("data.npz")
  d=f["d"]
  i=f["i"]
  
  
  idd=15+2
  
  draw(d[idd])
  
  print(i[idd])
  
  plt.show()




